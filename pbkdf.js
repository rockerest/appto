export {
	createFromPbkdf2 as upgradeToAES256
} from "./aes.js";

export async function createFromPassword( {
	password,
	usages = [ "deriveBits", "deriveKey" ],
	exportable = false
} ){
	var buffer = new TextEncoder( "utf-8" ).encode( password );

	return await crypto.subtle.importKey(
		"raw",
		buffer,
		"PBKDF2",
		exportable,
		usages
	);
}