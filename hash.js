import { md5 } from "./md5.js";

import { bufferToHex } from "./common.js";

export async function hash( { input, algorithm = "SHA-512" } ){
	var handlers = {
		"md5": ( str ) => md5( str ),
		"*": async ( str, algo ) => {
			let buffer = new TextEncoder( "utf-8" ).encode( str );

			return await crypto
				.subtle
				.digest( algo, buffer )
				.then( bufferToHex );
		}
	};
	var response;

	if( algorithm == "md5" ){
		response = handlers[ "md5" ]( input );
	}
	else{
		response = await handlers[ "*" ]( input, algorithm );
	}

	return response;
}