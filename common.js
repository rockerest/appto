export const DEFAULT_ITERATIONS = 1000000;

export function getSalt(){
	return crypto.getRandomValues( new Uint8Array( 16 ) );
}

export function getInitVector(){
	return crypto.getRandomValues( new Uint8Array( 12 ) );
}

export function bufferToHex( buffer ){
	return [ ...new Uint8Array( buffer ) ]
		.map( ( byte ) => byte.toString( 16 ).padStart( 2, "0" ) )
		.join( "" );
}

export function hexToBuffer( hexadecimal ){
	var view = new Uint8Array( hexadecimal.length / 2 );

	for( let i = 0; i < hexadecimal.length; i += 2 ){
		view[ i / 2 ] = parseInt( hexadecimal.substring( i, i + 2 ), 16 );
	}

	return view.buffer;
}