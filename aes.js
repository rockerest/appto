import {
	DEFAULT_ITERATIONS,
	getSalt,
	hexToBuffer,
	bufferToHex,
	getInitVector
} from "./common.js";
import { wrap, unwrap } from "./jwk.js";

export async function createFromPbkdf2( {
	pbkdf2,
	salt = getSalt(),
	iterations = DEFAULT_ITERATIONS,
	usages = [ "encrypt", "decrypt", "wrapKey", "unwrapKey" ],
	exportable = true
} ){
	var isPBKDF2 = pbkdf2.algorithm.name == "PBKDF2";
	var key;

	if( isPBKDF2 ){
		key = await crypto.subtle.deriveKey(
			{
				"name": "PBKDF2",
				"hash": "SHA-512",
				iterations,
				salt
			},
			pbkdf2,
			{ "name": "AES-GCM", "length": 256 },
			exportable,
			usages
		);
	}
	else{
		throw new Error( "Input key must be a PBKDF2 key" );
	}

	return {
		key,
		salt,
		iterations,
		usages
	};
}

export async function wrapAsJwk( {
	wrappable,
	wrapKey = wrappable
} ){
	return await wrap( {
		"key": wrappable,
		"algo": "AES-GCM",
		wrapKey
	} );
}

export async function unwrapFromJwk( {
	unwrappable,
	unwrapKey,
	iv
} ){
	return await unwrap( {
		"key": hexToBuffer( unwrappable ),
		"algo": "AES-GCM",
		"iv": hexToBuffer( iv ),
		unwrapKey
	} );
}

export async function encrypt( { text, key } ){
	var message = new TextEncoder( "utf-8" ).encode( text );
	var iv = getInitVector();
	var cipher = await crypto.subtle.encrypt(
		{
			"name": "AES-GCM",
			iv
		},
		key,
		message
	);

	return {
		"cipher": bufferToHex( cipher ),
		"iv": bufferToHex( iv )
	};
}

export async function decrypt( { cipher, key, iv } ){
	var plaintext = await crypto.subtle.decrypt(
		{
			"name": "AES-GCM",
			"iv": hexToBuffer( iv )
		},
		key,
		hexToBuffer( cipher )
	);

	return new TextDecoder( "utf-8" ).decode( plaintext );
}
