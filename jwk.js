import { bufferToHex, getInitVector } from "./common.js";

export async function wrap( { key, wrapKey, algo } ){
	var algorithms = {
		"AES-GCM": { "name": "AES-GCM", "iv": getInitVector() }
	};

	return {
		"iv": bufferToHex( algorithms[ algo ].iv ) || null,
		"key": bufferToHex( await crypto.subtle.wrapKey(
			"jwk",
			key,
			wrapKey,
			algorithms[ algo ]
		) )
	};
}

export async function unwrap( {
	key,
	algo,
	unwrapKey,
	iv,
	usages = [ "encrypt", "decrypt", "wrapKey", "unwrapKey" ]
} ){
	var algorithms = {
		"AES-GCM": { "name": "AES-GCM", iv }
	};

	return crypto.subtle.unwrapKey(
		"jwk",
		key,
		unwrapKey,
		algorithms[ algo ],
		algorithms[ algo ],
		true,
		usages
	);
}