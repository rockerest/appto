import {
	createFromPbkdf2,
	unwrapFromJwk,
	wrapAsJwk,
	encrypt as aesEncrypt,
	decrypt as aesDecrypt
} from "./aes.js";
import { createFromPassword } from "./pbkdf.js";
import { bufferToHex, hexToBuffer } from "./common.js";

export async function createUserKey( password ){
	var pbkdf2 = await createFromPassword( { password } );
	var aes = await createFromPbkdf2( { pbkdf2, "iterations": 10000 } );

	return aes;
}

export async function dehydrateUserKey( key ){
	var wrapped = await wrapAsJwk( { "wrappable": key.key } );

	return btoa( JSON.stringify( {
		...wrapped,
		"uks": bufferToHex( key.salt ),
		"uki": key.iterations
	} ) );
}

export async function hydrateUserKey( { dehydratedKey, password } ){
	var hydrated = JSON.parse( atob( dehydratedKey ) );
	var pbkdf2 = await createFromPassword( { password } );
	var aes = await createFromPbkdf2( {
		pbkdf2,
		"salt": hexToBuffer( hydrated.uks ),
		"iterations": hydrated.uki
	} );

	var key = await unwrapFromJwk( {
		"unwrappable": hydrated.key,
		"unwrapKey": aes.key,
		"iv": hydrated.iv
	} );

	return key;
}

export async function encrypt( { key, text } ){
	var encryption;

	if( !( key instanceof CryptoKey ) ){
		key = key.key;
	}

	encryption = await aesEncrypt( { text, key } );

	return btoa( JSON.stringify( encryption ) );
}

export async function decrypt( { encrypted, key } ){
	var expanded = JSON.parse( atob( encrypted ) );

	if( !( key instanceof CryptoKey ) ){
		key = key.key;
	}

	return await aesDecrypt( { ...expanded, key } );
}