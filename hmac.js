import { bufferToHex } from "./common.js";

export async function createFromPassword( {
	password,
	hash = "SHA-512",
	usages = [ "sign" ],
	exportable = false
} ){
	var buffer = new TextEncoder( "utf-8" ).encode( password );

	return await crypto.subtle.importKey(
		"raw",
		buffer,
		{
			"name": "HMAC",
			"hash": {
				"name": hash
			}
		},
		exportable,
		usages
	);
}

export async function signMessage( { message, key } ){
	var msgBuffer = new TextEncoder( "utf-8" ).encode( message );

	return await crypto.subtle
		.sign(
			"HMAC",
			key,
			msgBuffer
		)
		.then( bufferToHex );
}

export async function hmac( { password, message } ){
	var key = await createFromPassword( { password } );

	return await signMessage( { message, key } );
}